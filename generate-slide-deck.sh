echo 'starting reveal compile'

rm -rf reveal.js/images
mkdir reveal.js/images
cp -R src/images/* reveal.js/images
bundle exec \
  asciidoctor-revealjs  \
  -a slides \
  -B src \
  -D .. \
  -r asciidoctor-diagram \
  src/slides.adoc \
  --out-file reveal.js/index.html
echo 'ending reveal compile'
