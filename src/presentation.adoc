include::asciidoc-pdf.settings[]

= Chariot SPA Day Angular Presentation

== Intro

**[[ slide 1 ]]**

Good morning!

* Who's using Angular Already? Show your hands...

* And who's looking at various libraries and frameworks, and trying to make a decsision?


Well, Welcome!

My name is Rich Freedman

I'm a Consultant with Chariot Solutions, and I've been working with Angular
since it's first release just over two years ago.

I've been doing software development for about 30 years now,
and have been with Chariot for just about 11 years.

I've always considered myself a back-end developer, using languages like
C, C++, Java, and Scala, but I've almost always had to provide the
user interface myself.

And, since the start of the Web, that has generally
meant a fair amount of JavaScript, starting with raw JavaScript (we called it "dynamic html" back then,
and evolving to JQuery, then to Angular JS, and now I've been primarily doing Angular  consulting for the past two years.

So, I suppose that I should actually consider myself a "full stack" developer,
rather than a "back-end developer who writes some JavaScript".

If my background resonates with you, then you're likely to
hear things today about Angular that will make you want to evaluate it in more depth.

And hopefully, for those with different backgrounds, you will find this interesting as well.




In this session, we're going to take a look at Angular,
and explore some of its features and capabilities.

I'll try to present a balanced view of how to determine
if Angular would be a good fit for your development team,

and then we'll take a deeper dive, and look at some actual Angular code,
so that you can get a flavor of what is involved in developing an Angular SPA.

If there's time at the end, I'll take some questions.

And even if not, I'll be here all day - feel free to come ask me about Angular.

Also, there will be a "lively" Panel discussion at 4:00, with all of today's speakers - don't miss it!

**[[ end slide 1 ]]**


== What is Angular?

**[[ slide 2 ]]**

So, What Is Angular?

=== Angular is a full-fledged SPA framework

Angular is a full-fledged "Single Page Application Framework", and is sometimes referred to as a "platform".

This is one of the major things that distinguishes it from the other SPA development environments
that you'll hear about today.

As my Intro slide said, it's a "Batteries Included" Framework.

Angular is both very opinionated and quite comprehensive.

With a single install, you get everything that you need to build a basic SPA,
and most of the things that you will need to build more advanced applications.

When you use Angular, you don't have to make choices about things like the language to use,
the build framework, the loader, what Router to use, what Forms library, which Http Client to use, and so forth.

Those sorts of decisions have already been made, and are baked into the framework.

Some are easy to swap out for other libraries if you choose, while others are pretty hard-wired.

I'll talk more about this as we review Angular's features.

=== Sponsorship / License
Angular is is developed by a Team led by Google, and like the other SPA tools that are being presented today,
Angular is an Open Source Project, with an MIT license.

So, as with React, Vue, and Node itself, you shouldn't have to spend a lot of time thinking about license compatibility.

=== Origin / AngularJS
Angular is a complete rewrite of AngularJS.

While it was great from a marketing point of view to name this new framework "Angular",
it certainly is a pain to use a search engine to look for info about it.

To be clear, Angular 1.x is "AngularJS",
and everything later, Angular 2 through the current Angular 7 is just referred to as "Angular".

And while Angular is informed by it's predecessor AngularJS, and incorporates many "lessons learned",
they really are two fairly different frameworks.


=== Community

Angular has a large community (including the Angular Philly Meetup group),
and there are lots of resources available, both online, and at conferences.

There's a large number of high-quality third-party libraries available, both Open-Source and
from commercial vendors, and documentation and training are widely available.

And, of course, Chariot Solutions offers Training, Consulting, and Mentoring for Angular,
as well as for React and Vue.


**[[ end slide 2 ]]**




== What is Angular? (Cont'd)

**[[ slide 3 ]]**

=== Typescript
The Angular framework is written in Typescript, which, if you are not familiar with it,
is a statically typed superset of ES6.

It is a hybrid object-oriented, imperative, and functional language that transpiles to ES5.

Most Angular applications are written in Typescript.

You can theoretically write an Angular directly in Javascript, but few people actually do this,
and I wouldn't recommend it in an Enterprise environment.


=== Components

On a more technical level, Angular is a Component-Based SPA framework.
As you will see when we start looking at code, the framework encourages encapsulating small pieces of functionality
in components. This helps to enforce separation of concerns, and
helps to make the encapsulated code re-usable.

I should point out that Angular is not unique in this respect - React, for example, has the same concept,
and this is certainly not a ground-breaking concept in computer science.

=== Modules

Beyond Components, Angular also has the concept of Modules, which serve a similar purpose to,
but which should not be confused with JavaScript module patterns such as
IFFEs (Immediately Invoked Function Expressions), AMD, CommonJS, UMD, or ES5 or ES6 modules.

But, for a point of reference, let's compare Angular modules to ES6 modules, to which they are most similar.

While ES6 modules represent a single source file, Angular's Modules are logical groupings of features within
the Angular application, generally involving several source files.

Like ES6 modules, Angular modules specify what is exposed outside of the module (exported),
as well as what other modules they depend on.

These two concepts are referred to as "imports" and "exports"

They are, however, specific to Angular. When working with Angular, you will typically work with both Angular modules,
and ES6 module syntax.

Since ES6 is not yet well-supported by Browsers, Angular converts it's modules at compile-time
into the CommonJS module system, as it also transpiles the Typescript and ES6 constructs used in Angular
into ES5.

Also, Angular modules are heavily involved with defining the scope and behaviour of the Dependency Injector, which
is also a feature of the Angular framework.

Again, this is just a highlight - I promise that we'll get back to
the topic of Modules once we start looking at some code.

**[[ end slide 3 ]]**





== What does Angular Provide?

**[[ slide 4 ]]**

* Like many other modern development tools, including Node,
Angular has a Command-Line Interface, or CLI,
that can generate a complete starter application,
and can also add features to an existing application.

* Angular provides a Canonical project structure
  While it's fairly flexible, with the Angular-CLI, you generally won't have to think about where
  to put a particular piece of source code in the file system hierarchy. There are strong conventions
  for most things.

* RxJS 6 is used just about everywhere for Reactive, Asynchronous programming, not just for making HTTP calls,
but also for handling DOM events, interacting with the Router, notifying observers about form value changes
and other events - basically everywhere that you might otherwise use a Promise,
and in many places where you might not have thought of using one.

* Angular provides Server-Side Rendering capabilities via Angular Universal.
  * Server-side rendering can facilitate web crawlers. to enhance your SEO, if you are building a public-facing app,
  * It can improve the "first load time" of your application when used for the "landing page",
  * and, while it would take quite a bit of work,
    Angular claims that if you use server-side rendering for your entire app,
    you can greatly improve the performance of your application
    on low-power devices and those with slow network connections.

* Native Mobile Applications (both Andriod and iOS) can be developed with Angular, by using NativeScript.
The Angular team has worked closely with the NativeScript developers
to provide a very good NativeScript integration.

This is a relatively new way to build native mobile apps with web technologies.
NativeScript uses a JavaScript interpreter to run the logic of your Angular app,
but uses the native OS's UI directly, and makes the entire native API available.

It reportedly results in more responsive and performant applications, that have a
truly native look-and-feel than past solutions, such as Cordova and PhoneGap,
and directly competes with React Native.


Because Angular is not directly tied to Browser DOM rendering,
it is possible to write one Angular application for both the web and mobile,
which shares most of the code, and which has different code just for the views
(HTML and CSS files), and for the module definitions.

* Angular provides a pre-configured testing environment,
with unit and integration testing based on Jasmine and Karma,
and End-to-End testing with the addition of Protractor,
which uses Selenium to drive a web browser, such as ChromeHeadless


* The CLI provides the ability to package Angular modules
as NPM libraries so that they can be shared among applications.

Up until Angular 5, this was very difficult to do,
and besides the Angular team, was primarily done by
library vendors, and other folks with lots of time on their hands.

In late 2017, around the time of the Angular 5 release,
the ng-packagr library was released, and suddenly, mere mortals
could package their Angular modules as NPM modules.

In May of 2018, Angular 6 was released, and included the ng-packagr
library, with the CLI updated to make it even easier. Now, it's
pretty much a no-brainer to take a useful module from your Angular app,
and make it into a re-usable library.

By the way, you don't have to actually publish your NPM module
to the central NPM repository just to use it within your organization.
There are ways, including the use of the SonaType Nexus
or JFrog Artifactory  repositories (both usually used
to host Java libraries), to set up your own internal NPM repository.


== What does Angular Provide? (Cont'd)

**[[ slide 5 ]]**

Features Include

* Components (as previously mentioned), as well as

  - Directives, which, like Components, modify the view,
    but which do not have their own views
    (an example would be directive that highlights text of
    dom nodes that have particular attributes).

  - Pipes, which are objects that can transform values,
  and are usually referenced in views - such as a DatePipe,
  which takes in a date, and outputs a text representation of that date,
  in a particular format.

  - Services, which are basically any TypesScript class that you provide
  to the Dependency Injector by naming them in the "provides" section of a Module.

  If you are familiar with the Spring Java framework, this is very similar to
  using Spring's @Service annotation.

* Constructor-based Dependency Injection
  - Any class that has an @Injectable decorator, and a constructor that
    that takes one or more arguments, has it's constructor arguments injected,
    if that class itself is constructed by the dependency injector.

    This is the case for many of the classes that you will write in an
    angular application, such as Components, Directives, Pipes, Services, etc.

  - If you are a Java or C# developer, this should look very familiar to you.

* A very robust Router implementation. Angular's Router goes well beyond basic
navigation, and includes things like lazy loading of feature modules, exposure
of the Router State to routed components, Router event notification, and
Route Guards (in which user-written guard classes can make decisions about whether
a route can be navigated to, or whether a route can be left).

 ** Route guards can be used, for instance to prohibit navigation to a particular route
 if the user has not been authenticated by the API.

 ** Another common use-case is to prompt the user when they attempt to navigate
 away from a form that they have modified, but not saved. Since the Route Guards
 are RxJS Observable-based, the decision can be deferred, allowing you to pop up a dialog,
 and ask the user if they want to actually leave, or cancel, and stay on the current route.

* An Http Client, which is RxJS - based, and which speaks JSON by default
  - The HttpClient returns RxJS Observables, not Promises.

  If you're not familiar with RxJS, don't worry, I'll talk more about that later.

  - The HttpClientModule provides for HttpInterceptors, which can be used for
  things like adding headers to requests (for instance, adding authorization
  headers to all requests in one place, instead of repeating that on every
  http call, and centralized handling of HTTP error responses.

* Component-based HTML templates with CSS Encapsulation.
  As we will explore later, each Component has it's own HTML Template and
  CSS files. By default the Component's HTML and CSS are encapsulated so that
  the component's HTML is only affected by it's own CSS.

  - This can be selectively overridden, or completely turned off,
  on a per-component basis.


* Data Binding, with a Choice of one-way binding or two-way binding.
  - If you like the idea of state containers such as Redux,
    that's available for Angular too, and there are at least choices -
    an adapter for using Redux, and an Angular-specific reimplementation
    of the Redux concept, called ngrx/store.

* Forms: Angular has two different Forms modules
  - Template-Driven Forms, where fields are directly bound to members of the component,
    typically with two-way bindings,
    and the form is generally validated when the user "submits" it.

  - Then there are Reactive Forms, where the form holds it's own model,
    validator objects are attached, and validations
    happen reactively, as the user interacts with the form.

The are just the highlights, of course.
Angular provides many features, and is quite extensible.

**[[ end slide 5 ]]**




== Is Angular a Good Fit For Your Development Team?

**[[ slide 6 ]]**

If you are currently considering developing an SPA, you may
overwhelmed by the choices. Angular, React, Vue, etc.,

Why would you choose one over the other?

As Ken mentioned in his introduction, we have implemented the same application
in Angular, React, and Vue,. All three implementations use the same API from the same back-end service,
and they all look and act very similarly.

Granted, the application is relatively small and simple, but this shows that basically,
they can all get the job done.

The differences are, in some cases, somewhat stylistic. For instance, as you will see later,
Angular takes the approach of putting small snippets of code into HTML templates,
while React puts JSX templates in the code.

Despite some people's arguments to the contrary, I don't think that either approach
is clearly better than the other, but you may certainly prefer one way over the other.

However, there are also some significant differences, and depending on what kind of application you
are developing, and the background of your development team, there may be reasons why you would or would not choose
to use Angular.

Here are some things that would make Angular a Good Fit:

[step]
* If you have green-field development projects, Angular might be a good fit for you.
  - while there are ways to add Angular to an existing app, they're not particularly easy.
  - there are features coming to Angular in future versions that will make this easier,
     like the upcoming IVY render, but they are not final yet, and are certainly not mature.

[step]
* If You develop medium to large sized applications, Angular might be a good fit for you.
  - While Angular can be used for small applications, it really shines in the medium-to-large application space,
    particularly when there is a team of developers. If you are writing a bunch of small applications,
    you may still see some benefit, especially re-usability, with the ability to publish Angular NPM libraries
    and use them across applications.

[[ end slide 6 ]]



== Is Angular a Good Fit For Your Development Team? (Cont'd)

**[[ slide 7 ]]**

* If your development team is not heavily invested in pure JS, and wants the big infrastructure decisions
already made, Angular might be a good fit for you.

  ** If your team members are JS experts, and want complete control, they may not be happy with the idea of
  working in Typescript, or with having most of the architecture and major library decisions made for them.

  But, for a team that does not have a heavy investment in Javascript, and that does not want to spend
  a significant amount of time deciding on the architecture and libraries - like (which Router? which Forms Library?),
  Angular is a great fit, because it comes with the architecture and libraries already included - those decisions
  have already been made. It is indeed a Framework, with a capital 'F' - It is truly "Batteries Included".

* If your development team is already productive with an Object-Oriented back-end framework,
  such as Java/Spring, or C#/.NET, Angular might be a good fit for you.

  ** While Typescript is not strictly object oriented,
  the similarities are considerable, and can greatly flatten the team's learning curve.

  ** On the other hand, if your developers are currently doing Node-based back-end development in JavaScript,
  that's probably a clue that they will prefer something other than Angular and TypeScript.

**[[ end slide 7 ]]**




== Creating A Minimal Angular App with the Angular-CLI

**[[ slide 8 ]]**


 Let's Take a look at what it takes to  generate and run a minimal Angular Application:

 * First, you would install the Angular CLI.
 You'll only need to do this once, not every time you create a new application.

 npm install -g @angular/cli

 * Then, you can generate a complete, simple application and install it's dependencies, with one command:

 ng new awesome-project


 * You can then Run the app by going into the generated project's directory
 and either starting it directly via the CLI's 'ng serve' command,
 or by running it via an included npm 'start' script:

 cd awesome-project

 ng serve -o # or npm start

 And, of course, virtually every IDE can do this for you.

 **[[ end slide 8 ]]**



==== What gets generated?

**[[ slide 9 ]] (file listing)**

So, what gets generated?

- various configuration files -
   package.json, which is required for every node-based application,
   karma config, tsconfig, etc.

- environment configurations, assets (e.g. images)

- main.ts, which is a bit of TypeScript that actually bootstraps the
Angular application

- the source code for a single 'app' module, containing a single 'app' component, along with it's HTML template and CSS (or SCSS) file.

And, of course, an index.html file.

This is, of course, the file that the browser loads first when it encounters your Angular application.

It gets injected with the transpiled main.js and other runtime code,
and bootstraps the root Module, which is the AppModule.

The AppModule in turn loads any other module dependencies,
and mounts the AppComponent, and your application is up and running.

**[[ end slide 9 ]]**



====== App Component  ======

**[[ slide 10 ]]**

- Angular generates a very minimal 'AppComponent', whose entire purpose is to render the persistent
  'frame' of the UI, in which other components are mounted, possibly along with a
  thing known as a Router Outlet, which we'll explore later.

- Since the simple app that is generated isn't very interesting,
so I'll be showing code from the Angular implementation of our "SPA Day" app,
which is also small, but hopefully more interesting.


[source, javascript]
----
import {Component} from '@angular/core';

@Component({
  selector: 'spa-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.scss'
  ]
})
export class AppComponent {
}
----

* Note that there is no HTML or CSS in this file - they are in external files (but may be in-lined, if small),
and because it's just a 'frame', the class has no members or methods. In some applications, you might
add code here, such as for toggling a side-menu.

* Also note the use of the "@Component" decorator.
This marks a class as an Angular component and provides configuration metadata.

 Decorators are a proposed feature for JavaScript and a current experimental feature
 of TypeScript, but are heavily used by the Angular framework.

 If you have used Annotations in Java, or Attributes in C#, decorators are somewhat similar in concept.
 They are used to associate meta-data with the items that are decorated.

* In this case, besides marking the class as a Component, the decorator
specifies the name of the 'selector', or custom tag name that will be used to mount this component,
  - here it is "spa-root",

* It also specifies the location of the component's HTML template, and the location of the
CSS files to be applied to the template - `./app.component.html` and `./app.component.scss` respectively

* While these files could be anywhere, it is idiomatic in Angular to name them with the
same root names as the component, and to place them in the same directory as the component.

* Also note that the Component specifies it's stylesheet as an SCSS file.

** This is directly supported by Angular-CLI, and while CSS is the default,
use of SCSS is a minor configuration, which you can specify on the command-line when creating the app with the CLI.


**[[ end slide 10 ]]**



===== App Component Template =====

**[[ slide 11 ]]**

The App Component's HTML template looks like this:

[source, html]
----
<div class="header">
  <img class="spaLogo" src="assets/head.png">
  <span class="title">Chariot SPA Day</span>
  <img class="chariotLogo" src="assets/chariot.svg">
</div>
<div class="app-container">
  <div class="home">
    <div class="row">
      <div class="column">
        <h2>Welcome to the SPA</h2>
        <spa-welcome></spa-welcome>
      </div>

      <div class="column">
        <router-outlet></router-outlet>
      </div>
    </div>
  </div>
  <spa-chat></spa-chat>
</div>
----

===== Custom HTML Elements for Selectors =====

This is valid HTML, but note the use of Custom HTML elements.

These correspond to the 'selector'attributes of the Components,
and cause the components to be instantiated and to render into the DOM at that point.

For example, the `<spa-welcome>` tag, which you can see here directly under the `H2` tag,
mounts the `WelcomeComponent`, whose `selector` attribute is `spa-welcome`.

===== Router Outlet =====

In this example, you also see `<router-outlet>` tag in the AppComponent's HTML template.
This is present if you have created the app with the routing feature, or have added it after the fact.
The `<router-outlet>` is where components will appear when they have been routed to.

**[[ end slide 11]**



==== Rendered WelcomeComponent ( <spa-welcome> )

**[[ slide 12]]**

The rendered result of the spa-welcome tag looks like this:

[source, html]
----
<spa-welcome _ngcontent-c0="" _nghost-c1="">
    <div _ngcontent-c1="" class="welcomeMessage">
        <div _ngcontent-c1="">
            Just float and wait for the wind to blow you around.
            Here's another <strong _ngcontent-c1="">little happy bush</strong>.
            Let all these little things happen. Don't fight them.
            Learn to use them. No worries. No cares.
        </div>
    </div
</spa-welcome>
----


===== Style Encapsulation =====

Note that all of the DOM nodes have custom attributes, generated by the framework, for example, `_ngcontent-c1`

This serves to "namespace" nodes with respect to styles, so that the styles are encapsulated by the component.

Styles from outside of the component's styles can be made to affect the component's nodes,
but this is not recommended, and is intended to be deprecated soon.

**[[ end slide 12]]**

====== App Module ======
**[[ slide 13]]**

[source, javascript]
----
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    WelcomeModule,
    ScheduleModule,
    RegistrationModule,
    ConfirmationModule,
    ChatModule,
    AngularFontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
----


Probably the most interesting part that is generated by the CLI is the AppModule.

As I mentioned previously, the module serves to encapsulate it's content, and is involved in dependency injection.

The AppModule **declares** Components that it encapsulates. This essentially makes the module a factory for
the declared component. Here you see that the `AppModule` **declares** the `AppComponent`

The AppModule also "imports" other modules, giving it's members access to Components exported by those modules.

Here, you can see that the AppModuule **imports** Angular's `BrowserModule`,
which supplies Angular's api, wrapped in a web-browser-specific interface.

If were writing a NativeScript application, you would use `NativeScriptModule` here instead.

It also imports the local AppRoutingModule, which supplies the application's Routes.

It then imports all of the application's Modules that appear in the Routes, making them available to the RoutingModule.

It also imports the ChatModule, whose ChatComponent is referenced in the app module's html template,
and the AngularFontAwesomeModule, which makes FontAweseome available throughout the application.

In the "providers" section, the module "provides" any Services,
which are essentially anything that's not a Component or one of a handful of other
types. This makes them available to the Dependency Injector.

The root AppModule is a special case, where any Services provided are made available to the dependency
injector in all modules throughout the application.

This application does not provide any services.

For the AppModule only, you see ``bootstrap: [AppComponent]``, which tells Angular which component to
insert into index.html

The module may also 'export' declared Components to make them available to other modules.
Typically there are no exports from the root module.

Finally, you will see an ES6 - style export, i.e. ``**export class AppModule** { }``
This is a Typescript / Javascript export, not an Angular export.

**[[end slide 13]]**



**[[ slide 14]]**

====== Router ======
The Router is built in to the Angular framework.
It's optional (you have to import it), and it is theoretically
possible to use a different implementation, but that's not something
that's commonly done.

Here, you see the Routing Module for the demo app.
This is just a convention. The "routes" configuration could
be defined directly in the app module, but is often kept separate this way.

The router routes to named Components, and in this simple setup,
the named Component will render into the app component's <router-outlet>.

It is possible to have router outlets lower in the hierarchy of components,
where a feature module may have a "main component", sometimes referred to as a
"container component", and that component has a router and a router outlet.
This module-specific router can then route to other components in the module,
and they will render into it's router outlet.

In larger applications, this is a very common practice.

[source, javascript]
----
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ScheduleComponent } from './schedule/schedule.component';
import { RegistrationComponent } from './registration/registration.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';

const routes: Routes = [
  { path: '', component: ScheduleComponent },
  { path: 'registration/:sessionId', component: RegistrationComponent },
  { path: 'confirmation/:email/:sessionId', component: ConfirmationComponent },
  { path: '**', redirectTo: '/'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

----

**[[end slide 14]]**

==== HttpClient / Observables ====

**[[slide 15]]**

Another library that is included with Angular is the HttpClient,
which is the class that is used to make Http Requests.

Angular's HttpClient is configured by default to send and receive JSON.
By default, it sets the content-type header to "application/json",
and by default, will attempt to parse the result as JSON.

This is often what you want, but of course not always.
The HttpClient is flexible - you can specify both the outbound
header (as well as any additional headers), and tell it how to treat the result,
including just giving you access to everything, including the response body and the headers.

When the client parses a JSON result, by default, you just get an "Object"
with properties mapped from the JSON.

You can (and usually will) indicate the type of the response with a TypeScript
type parameter.

Note that this does not actually create an instance of the indicated
interface or class, it just attempts to coerce the plain Object
into that type, and as long as the shape of the JSON matches the shape
of the indicated type, the parse will be successful, and you can use
the resulting object's properties as if it were the indicated type.

The resulting object, will not, however, have the indicated type's prototype,
so will not have it's methods and will not be an "instanceof" the type.

Here is a simple example of an Http call in the demo app:

[source, javsascript]
----
  getSession(id: number): Observable<Session> {
     return this.http.get<Session>(`${environment.apiUrl}/session/${id}`);
  }
----

This is a trivial example, and so not much different from using promises,
but RxJS is quite powerful when you need to make multiple, dependent requests,
and allows for additional operations such as retry.

**[[end slide 15]]**


**[[slide 16]]**

Here's a more complex request from the sample app, specifically,
from the schedule service, and it's "getSchedule" method.

[source, javsascript]
----
  getSchedule(): Observable<Session[]> {
    return this.http.get<Session[]>(`${environment.apiUrl}/session`).pipe(
      mergeMap( schedule =>
         forkJoin(schedule.map(session =>
          this.getRegistrationsForSession(session.id).pipe(
            map((registrations) =>
                <Session> {...session, registrationCount: registrations.length}
            )
          )
        ))
      )
    );

----

In this case, we want the list of sessions, and we also want the number of
registrations for each session.

In a real-world application, were we control what the API that the back-end
provides, we would just make sure that the API returns the counts along with
the sessions, or we might have it actually return the list of registrations
as an array in each session. But we didn't do that...

In some cases, you wind up using someone else's API, and it doesn't have
exactly what you want, and you have to make it work.

We could have fetched all of the sessions and all of the recipients,
and matched them up ourselves.

But, mostly as an excuse to show off what can be done with RxJS,
we did almost the least performant thing that we could think of.

We fetch all of the sessions, and then for each session, we fetch
the just the registrations for that session.

I say "almost" the least performant thing, because instead of
getting the registrations one session at a time, we get them
in parallel, or at least as parallel as possible given how many
http connections are available.

Finally, this code maps the Session and Registration Count pairs
into Sessions with Registration Counts.

We don't have time here today to go into deeper exploration of RxJS,
but I think that it is an extremely useful tool, and I'm not surprised that Angular
has used it throughout the framework.

**[[end slide 16]]**


==== Subscribing To an Observable ====

**[[ slide 17 ]]**

[source, javascript]
----
  private getSchedule() {
    this.scheduleService.getSchedule().pipe(delay(2000))
      .subscribe(
        schedule => this.schedule = schedule,
        err => console.log('Oops - failed to get schedule', err)
      );
  }
----

 This code shows the getSchedule method of the ScheduleComponent.
 It invokes the ScheduleService's getSchedule method that we saw on the previous slide,
 and gets back an Observable of Session.

 It uses the Observable's "pipe" method to introduce a delay (to simulate a slow network),
 and then subscribes to the Observable.

 As you can see, the subscribe method is passed two callback functions.
 The first receives the result when the call is succesful, and the second
 receives any errors, regardless of where the error may have happened in that
 complex set of http calls that the service made.

 These are very powerful concepts - both being able to use standard operators
 such as mergeMap and forkJoin to shape the way that observables proceed with respect to each other,
 and being able to handle errors from the composed observables in a single error handler.

*[[ end slide 17 ]]*


===== Directives =====

**[[ slide 18 ]]**

Directives are marked by the @Directive decorator

Directives are like Components, but without templates.

Technically, Components are Directives, but with templates

There are two types of Directives (besides Components):
Structural Directives, and Attribute Directives.

Structural Directives modify the structure of the View.
The most often-used built-in structural directives are
NgIf, which does conditional rendering, and NgFor,
which iterates over array-like data, and renders once
for each element.

**[[ end slide 18]]**


==== Use of Structural Directives
*[[ slide 19 ]]**

Here, in this snippet from the Schedule Component's view,
we can see the use of both the NgIf and the NgFor directive.

In the first line, the NgIf is used to only render the schedule if the schedule data exists,
and in the third line, the NgFor is used to iterate over the sessions in the schedule,
so that each will be displayed.

**[ end slide 19]]**

===== Pipes =====
**[ slide 20 ]]**

Pipes are classes that transform values, usually returning a string.
They are usually used in HTML views.

Probably the most frequently used built-in pipe is the DatePipe,
which takes a date and formats it as a string.

This example from the SPA-Day app is a custom Date Pipe, called "ShortDatePipe"
It takes in a Date, and formats it as a string containing
the Month Name and the day of the month.

Unlike Components, Pipes do not have a "selector" attribute,
but we still need a way to reference them. So Pipes have a "name" property
in their decorator, and the name for the ShortDatePipe is 'shortDate'

**[ end slide 20]]**


===== Usage of Pipes =====
**[ slide 21 ]]**

In this snippet from the Schedule Component's HTML template,
we can see the use of the 'shortDate' pipe, with the Session's
date being piped through it.

You can also see the shortTime pipe, which is another custom
date pipe in the application, used to format and output the time
of the session.

**[ end slide 21 ]]**


==== Angular Take-Awats ====

**[[ slide 22 ]]**

 Here are some things that I hope you will take away from this presentation.

 * Angular is a full-featured SPA Framework
 * Good FIt for Dev Teams without attachment to Pure JS
 * Good Fit for Dev Teams currently using OO back-ends
 * Good Fit for Medium-to-Large Applications
 * Large Community
 * Lots of Resources
 * Code is Written in TypeScript
 * Views are written (mostly) in HTML
 * RxJS is used Everywhere
 * Easy to create libraries from Components, Services, etc. and reuse them
 * Can be used for both Web and Native Mobile Development


**[[ end slide 22 ]]**


There's much more to Angular than what I've talked about today,
but I hope that I have given you enough of a flavor of the framework
so that you have an idea of what it is, and what it's like to work with

As you listen to the presentations on the other SPA tools today, you
should be able get a feel for how they compare, and which one is right for your SPA development needs.


==== If there's time,
 "With the time left, I'll try to answer some questions from the audience...."

==== If no time left,
"If anyone has any questions about Angular, feel free to ask me or Ken, or to contact Chariot after today's presentation"


=== TODO If enough time ===

==== event handlers (e.g. click events)
==== Forms (template and reactive) and Binding ====
